# SMENA Printer API

Сервис печати чеков с заказами

(Использует python 3.9)

## Установка

### 1) Инфраструктура

Инфраструктурные вещи необходимые для сервиса
(PostgreSQL, Redis, wkhtmltopdf) запускаются через docker-compose.

Дистрибутив:
https://bitbucket.org/mks_rozhkov/checks_service_infrastructure

```bash
# клонирование репозитория
git clone https://mks_rozhkov@bitbucket.org/mks_rozhkov/checks_service_infrastructure.git
cd checks_service_infrastructure

# сборка и запуск образов docker
docker-compose up -d --build
```

### 2) Сервис

Сервис не будет работать в Windows из-за [ограничений](https://python-rq.org/docs/#limitations)
библиотеки [rq](https://github.com/rq/rq).

```bash
# клонирование репозитория
git clone https://mks_rozhkov@bitbucket.org/mks_rozhkov/checks_service.git
cd checks_service

# создание и активация виртуальной среды
python -m venv venv
source venv/bin/activate

# установка зависимостей
pip install -r requirements.txt

# миграция моделей в БД
python manage.py makemigrations api
python manage.py migrate
```

## Запуск

### 1) django-приложение (development web server)

```bash
# активация виртуальной среды
source venv/bin/activate

# запуск веб-сервера
python manage.py runserver
```

### 2) Redis Queue - worker

#### 2.1) запуск через manage.py

```bash
python manage.py rqworker
```

#### 2.2) rqworker service (только в Ubuntu)

Создать файл

```bash
sudo nano /etc/systemd/system/rqworker.service
```

Сохранить в нем:

```bash
[Unit]
Description=Django-RQ Worker
After=network.target

[Service]
WorkingDirectory=<<path_to_your_project_folder>>
ExecStart=<<path_to_your_project_folder>>/venv/bin/python \
    <<path_to_your_project_folder>>/manage.py \
    rqworker

[Install]
WantedBy=multi-user.target
```

Активировать и запустить сервис

```bash
sudo systemctl enable rqworker
sudo systemctl start rqworker
```

## Использование

#### Панель администратора
Для доступа необходимо создать суперпользователя
```bash
python manage.py createsuperuser
```
> http://127.0.0.1:8000/admin/

#### Администрирование очереди Django RQ

> http://127.0.0.1:8000/django-rq/

### Методы API для ERP и приложения:

#### _Подробное описание доступных методов находится в файле [api.yml](https://github.com/smenateam/assignments/blob/master/backend/api.yml) в задании_

Создание чеков для заказа
> http://localhost:8000/create_checks/

Список доступных чеков для печати
> http://localhost:8000/new_checks/

PDF-файл чека
> http://localhost:8000/check/

## Тестирование
_Запуск тестов осуществлять с отключенным rqworker_

```bash
python manage.py test
```
