from django.db import models

check_type_choices = [('kitchen', 'Кухня'), ('client', 'Клиент')]
check_status_choices = [('new', 'Новый'), ('rendered', 'Подготовлен к печати'), ('printed', 'Напечатан')]


class Printer(models.Model):
    name = models.CharField(max_length=255, verbose_name='Название принтера')
    api_key = models.CharField(max_length=255, unique=True, verbose_name='Уникальный ключ')
    check_type = models.CharField(max_length=255, choices=check_type_choices, verbose_name='Тип чека')
    point_id = models.IntegerField(verbose_name='Идентификатор точки')

    class Meta:
        verbose_name = 'Принтер'
        verbose_name_plural = 'Принтеры'

    def __str__(self):
        return self.name


class Check(models.Model):
    printer_id = models.ForeignKey(
        'Printer',
        on_delete=models.CASCADE,
        related_name='checks',
        verbose_name='Название принтера'
    )
    type = models.CharField(max_length=255, choices=check_type_choices, verbose_name='Тип чека')
    order = models.JSONField(verbose_name='Заказ')
    status = models.CharField(max_length=255, choices=check_status_choices, verbose_name='Статус')
    pdf_file = models.FileField(upload_to='pdf/', null=True, blank=True, verbose_name='PDF файл')

    class Meta:
        verbose_name = 'Чек'
        verbose_name_plural = 'Чеки'

    def __str__(self):
        return f'Чек: {self.id} для принтера {self.printer_id}'
