from django.urls import path
from . import views

urlpatterns = [
    path('create_checks/', views.create_checks),
    path('new_checks/', views.get_new_checks),
    path('check/', views.print_check),
]
