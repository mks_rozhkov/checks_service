from django.contrib import admin

from .models import Printer, Check


@admin.register(Printer)
class PrinterAdmin(admin.ModelAdmin):
    list_display = ('name', 'api_key', 'check_type', 'point_id')
    list_filter = ('point_id', 'check_type')


@admin.register(Check)
class CheckAdmin(admin.ModelAdmin):
    list_display = ('id', 'printer_id', 'type', 'order', 'status', 'pdf_file')
    list_filter = ('printer_id', 'type', 'status')
    readonly_fields = ('type', )  # тип чека зависит от типа принтера

    def has_add_permission(self, request):  # возможность создать новый чек из админки отключена
        return False
