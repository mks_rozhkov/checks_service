import json

import django_rq
from django.db.models import Q, QuerySet
from django.http import JsonResponse, FileResponse
from django.views.decorators.csrf import csrf_exempt
from pydantic.error_wrappers import ValidationError

from .exceptions import ChecksAlreadyCreatedException, NoPrintersException
from .models import Printer, Check
from .pydantic_models import Order
from .tasks import create_and_save_pdf


def _inspect_checks_already_created(order: Order, printers: QuerySet) -> bool:
    """Возвращает bool(уже созданы чеки для заказа или нет)"""
    created_checks = Check.objects.filter(order=order.dict()).all()
    if created_checks:
        created_checks_printers = {check.printer_id.id for check in created_checks}
        point_printers = {printer.id for printer in printers}
        return created_checks_printers == point_printers
    else:
        return False


def _save_checks(order: Order) -> list[Check]:
    """Сохраняет новые чеки для заказа или вызывает исключение"""
    point_printers = Printer.objects.filter(point_id=order.point_id)
    if not point_printers:
        raise NoPrintersException
    if _inspect_checks_already_created(order, point_printers):
        raise ChecksAlreadyCreatedException
    return Check.objects.bulk_create(
        [Check(printer_id=printer,
               type=printer.check_type,
               order=order.dict(),
               status='new') for printer in point_printers]
    )


# Если ERP-система позволяет, лучше c POST запросом посылать CSRF cookie
# принудительно вернуть CSRF cookie вместе с response - @ensure_csrf_cookie
@csrf_exempt  # csrf защита отключена. TODO уточнить
def create_checks(request):
    if request.method == 'POST':
        try:
            json_data = json.loads(request.body)
            new_order = Order.parse_obj(json_data)
            created_checks = _save_checks(new_order)

            queue = django_rq.get_queue(is_async=True)
            for check in created_checks:
                queue.enqueue(create_and_save_pdf, check)

            return JsonResponse({"ok": "Чеки успешно созданы"})

        except (json.JSONDecodeError, ValidationError):
            return JsonResponse({"error": "Не удалось обработать JSON"}, status=400)
        except ChecksAlreadyCreatedException:
            return JsonResponse({"error": "Чеки для данного заказа уже были созданы"}, status=400)
        except NoPrintersException:
            return JsonResponse({"error": "У точки нет ни одного принтера"}, status=400)

    else:
        return JsonResponse({"error": "Воспользуйтесь методом POST"}, status=400)


def get_new_checks(request):
    if request.method == 'GET':
        api_key = request.GET.get('api_key')
        printer = Printer.objects.prefetch_related('checks').filter(api_key=api_key).first()
        if not printer:
            return JsonResponse({"error": "Ошибка авторизации"}, status=401)

        checks = [{"id": check.id} for check in printer.checks.filter(Q(status='rendered') | Q(status='printed'))]
        return JsonResponse({"checks": checks})

    else:
        return JsonResponse({"error": "Воспользуйтесь методом GET"}, status=405)


def print_check(request):
    if request.method == 'GET':
        api_key = request.GET.get('api_key')
        check_id = request.GET.get('check_id')

        printer = Printer.objects.prefetch_related('checks').filter(api_key=api_key).first()
        if not printer:
            return JsonResponse({"error": "Ошибка авторизации"}, status=401)

        requested_check = printer.checks.filter(id=check_id).first()
        if not requested_check:
            return JsonResponse({"error": "Данного чека не существует"}, status=400)

        if requested_check.status == 'new':
            return JsonResponse({"error": "Для данного чека не сгенерирован PDF-файл"}, status=400)

        requested_check.status = 'printed'
        requested_check.save()
        return FileResponse(open(requested_check.pdf_file.path, 'rb'))  # as_attachment=True TODO уточнить

    else:
        return JsonResponse({"error": "Воспользуйтесь методом GET"}, status=405)
