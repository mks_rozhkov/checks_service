import json
import os
from base64 import b64encode

import requests
from django.conf import settings


def create_pdf_binary_content(html_code: str) -> bytes:
    """
    Принимает html-код. Отправляет его с запросом на docker-контейнер wkhtmltopdf.
    Возвращает полученные от wkhtmltopdf бинарные данные сгенерированного PDF-файла
    """

    url = 'http://localhost:8080/'
    save_directory = settings.MEDIA_ROOT / 'pdf/'

    if not os.path.exists(save_directory):
        os.makedirs(save_directory)

    byte_content = bytes(html_code, 'utf8')

    base64_bytes = b64encode(byte_content)
    base64_string = base64_bytes.decode('utf8')

    data = {'contents': base64_string}

    headers = {'Content-Type': 'application/json'}

    response = requests.post(url, data=json.dumps(data), headers=headers)

    return response.content
