import django_rq
from django.test import TestCase, Client

from .models import Printer, Check

base_test_order = {
    "id": 123456,
    "price": 780,
    "items": [
        {
            "name": "Вкусная пицца",
            "quantity": 2,
            "unit_price": 250
        },
        {
            "name": "Не менее вкусные роллы",
            "quantity": 1,
            "unit_price": 280
        }
    ],
    "address": "г. Уфа, ул. Ленина, д. 42",
    "client": {
        "name": "Иван",
        "phone": "9173332222"  # в api.yml phone: type: string
    },
    "point_id": 1
}

queue = django_rq.get_queue('default', is_async=True)


def valid_orders_generator():
    """Генератор возвращает по одному валидному заказу для каждой имеющейся точки"""
    points_ids = {printer.point_id for printer in Printer.objects.all()}
    order_id = 1000

    for point_id in points_ids:
        test_order = base_test_order.copy()
        test_order.update({'id': order_id, 'point_id': point_id})
        yield test_order
        order_id += 1


class CreateChecksTestClass(TestCase):
    """Тесты для create_checks/"""

    fixtures = ['printer.json']

    @classmethod
    def setUpTestData(cls):
        cls.client = Client()

    @classmethod
    def tearDownClass(cls):
        queue.delete(delete_jobs=True)
        Check.objects.all().delete()

    def test_create_checks_valid(self):
        """Проверка создания чеков с валидными данными"""

        for valid_order in valid_orders_generator():
            response = self.client.post('/create_checks/', valid_order, content_type='application/json')

            # проверка ответа от сервера
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.json(), {"ok": "Чеки успешно созданы"})

            # проверка количества созданных чеков на точке (на каждой точке в fixtures - 2 принтера)
            created_checks = Check.objects.filter(printer_id__point_id=valid_order['point_id'])
            print(created_checks)
            self.assertEqual(len(created_checks), 2)

            # проверка полей каждого чека
            for check in created_checks:
                self.assertEqual(check.status, 'new')
                self.assertFalse(bool(check.pdf_file))
                self.assertEqual(check.type, check.printer_id.check_type)
                self.assertEqual(check.order, valid_order)

    def test_create_checks_invalid(self):
        """Проверка создания чеков с невалидными данными"""

        invalid_point_id = 99
        # проверка, что в базе нет принтеров с таким point_id
        self.assertFalse(bool(Printer.objects.filter(point_id=invalid_point_id)))

        invalid_order = base_test_order.copy()
        invalid_order.update({'point_id': invalid_point_id})
        response = self.client.post('/create_checks/', invalid_order, content_type='application/json')

        # проверка ответа от сервера
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {"error": "У точки нет ни одного принтера"})

    def test_create_checks_duplicate(self):
        """Проверка попытки повторного создания заказа с одним и тем же JSON"""

        gen = valid_orders_generator()
        single_valid_order = next(gen)  # получаем один валидный заказ

        # первый запрос приводит к созданию чеков
        response = self.client.post('/create_checks/', single_valid_order, content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {"ok": "Чеки успешно созданы"})

        # второй запрос должен вернуть ошибку
        response = self.client.post('/create_checks/', single_valid_order, content_type='application/json')
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {"error": "Чеки для данного заказа уже были созданы"})


class ChecksTestClass(TestCase):
    """Тесты для new_checks/ и check/"""

    fixtures = ['printer.json', 'check.json']

    @classmethod
    def setUpTestData(cls):
        cls.client = Client()

    @classmethod
    def tearDownClass(cls):
        queue.delete(delete_jobs=True)
        Check.objects.all().delete()

    def test_new_checks_valid_api_keys(self):
        response = self.client.get('/new_checks/', {'api_key': 'api_key_1'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {'checks': [{'id': 1}]})

        response = self.client.get('/new_checks/', {'api_key': 'api_key_3'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {'checks': []})

    def test_new_checks_invalid_api_key(self):
        response = self.client.get('/new_checks/', {'api_key': 'not_valid_api_key'})
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.json(), {'error': 'Ошибка авторизации'})

    def test_check_valid(self):
        """Проверка полного цикла создания заказа, генерации и получения PDF"""

        queue.delete(delete_jobs=True)

        valid_order = base_test_order.copy()
        valid_order.update({'id': 2000, 'point_id': 2})

        # отправка нового заказа, проверка ответа
        response = self.client.post('/create_checks/', valid_order, content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {"ok": "Чеки успешно созданы"})

        # проверка статуса заказа
        created_checks = Check.objects.filter(printer_id__point_id=2)
        for check in created_checks:
            self.assertEqual(check.status, 'new')

        # выполнение заданий по созданию PDF из очереди rq
        worker = django_rq.get_worker()
        worker.work(burst=True)

        created_checks = Check.objects.filter(printer_id__point_id=2)
        for check in created_checks:
            # проверка изменения статуса заказа
            self.assertEqual(check.status, 'rendered')

            # проверка получения PDF в ответе сервера
            response = self.client.get('/check/', {'api_key': check.printer_id.api_key, 'check_id': check.id})
            self.assertEqual(response.status_code, 200)
            self.assertEquals(response.get('Content-Disposition'),
                              f'inline; filename="{valid_order["id"]}_{check.type}.pdf"')

            # проверка изменения статуса заказа

            self.assertEqual(Check.objects.get(id=check.id).status, 'printed')

        # принудительное удаление записей для удаления созданных файлов (через signals)
        created_checks.delete()

    def test_check_invalid_api_key(self):
        response = self.client.get('/check/', {'api_key': 'not_valid_api_key', 'check_id': 1})
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.json(), {'error': 'Ошибка авторизации'})

    def test_check_invalid(self):
        response = self.client.get('/check/', {'api_key': 'api_key_1', 'check_id': 9999})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {"error": "Данного чека не существует"})

    def test_check_not_ready(self):
        response = self.client.get('/check/', {'api_key': 'api_key_1', 'check_id': 3})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {"error": "Для данного чека не сгенерирован PDF-файл"})
