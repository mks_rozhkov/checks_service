class ChecksAlreadyCreatedException(Exception):
    """Вызывается, если чеки для поступившего заказа уже созданы"""
    pass


class NoPrintersException(Exception):
    """Вызывается, если для указанной в заказе точки отсутствует хотя бы один принтер"""
    pass
