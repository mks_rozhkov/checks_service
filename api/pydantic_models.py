from pydantic import BaseModel


class Item(BaseModel):
    name: str
    quantity: int
    unit_price: int  # or float / decimal? TODO убедиться, что все цены без копеек


class Client(BaseModel):
    name: str
    phone: str


class Order(BaseModel):
    id: int
    price: int  # or float / decimal?
    items: list[Item]
    address: str
    client: Client
    point_id: int
