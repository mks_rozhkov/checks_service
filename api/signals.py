import os

from django.db.models.signals import post_delete, pre_save
from django.dispatch import receiver

from .models import Check


@receiver(post_delete, sender=Check)
def delete_pdf_on_check_delete(sender, instance, **kwargs):
    """Удаление файла pdf при удалении записи Check"""
    if instance.pdf_file:
        if os.path.isfile(instance.pdf_file.path):
            os.remove(instance.pdf_file.path)


@receiver(pre_save, sender=Check)
def delete_pdf_on_check_update(sender, instance, update_fields, **kwargs):
    """Удаление файла pdf при изменении поля pdf_file записи Check"""
    if not instance.pk:
        return False

    try:
        old_pdf_file = sender.objects.get(pk=instance.pk).pdf_file
    except sender.DoesNotExist:
        return False

    new_pdf_file = instance.pdf_file
    if old_pdf_file and old_pdf_file != new_pdf_file:
        if os.path.isfile(old_pdf_file.path):
            os.remove(old_pdf_file.path)
