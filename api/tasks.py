import io

from django.core.files import File
from django.template.loader import render_to_string

from .models import Check
from .wkhtmltopdf import create_pdf_binary_content


def _render_check_html(template: str, context: dict) -> str:
    """Возвращает rendered template в виде строки"""
    context_to_render = {'order': context}
    return render_to_string(template_name=template, context=context_to_render)


def create_and_save_pdf(check: Check) -> str:
    """Сохраняет сгенерированный PDF-файл в поле check.pdf_file, меняет check.status на 'rendered'"""
    templates = {'client': 'client_check.html', 'kitchen': 'kitchen_check.html'}
    html_code = _render_check_html(template=templates[check.type], context=check.order)
    filename = f'{check.order["id"]}_{check.type}.pdf'

    pdf_data = create_pdf_binary_content(html_code)
    pdf_io = io.BytesIO(pdf_data)
    check.pdf_file.save(filename, File(pdf_io))
    check.status = 'rendered'
    check.save()

    return check.pdf_file.path
